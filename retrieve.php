<?php
require __DIR__ ."/db.php";


$inputData = retrieve();
$headers = ["EventID", "LotNumber", "Category", "Region", "Title", "Subtitle", "Description", "Price", "ReservePrice", "FixedPrice", "AcceptOffers", "Image_1", "Image_2", "Image_3", "Image_4", "Image_5", "Image_6", "Image_7", "Image_8", "Image_9", "Image_10", "Image_11", "Image_12", "Image_13", "Image_14", "Image_15", "YouTubeID", "PdfAttachments", "Bold", "Badge", "Highlight", "ShippingOptions"];

$fields = ["EventID", "lot_number", "category", "Region", "title", "sub_title", "description", "Price", "ReservePrice", "FixedPrice", "AcceptOffers", "Image_1", "Image_2", "Image_3", "Image_4", "Image_5", "Image_6", "Image_7", "Image_8", "Image_9", "Image_10", "Image_11", "Image_12", "Image_13", "Image_14", "Image_15", "YouTubeID", "PdfAttachments", "Bold", "Badge", "Highlight", "ShippingOptions"];

$rows = [$headers];

foreach ($inputData as $key => $data) {
	$cols=[];
	foreach ($fields as $key => $field) {
		if (array_key_exists($field, $data)) {
			$cols[]=$data[$field];
		}else{
			$cols[]="";
		}
	}
	$rows[]=$cols;
}

outputCsv("Data.csv",$rows);
// echo json_encode($rows);