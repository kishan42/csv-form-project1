<?php
// $dbh = new PDO('mysql:host=localhost;dbname=test', "root", "");
ini_set("session.cookie_lifetime",(60 * 60 * 24 * 365)."");
session_start();

function store($data){
	$_SESSION['data'][] = $data;
}

function retrieve(){
    if (!isset($_SESSION['data'])) {
        $_SESSION['data']=[];
    }
	return $_SESSION['data'];
}

function clear()
{
	$_SESSION['data']=[];
}

function debug($data,$die=true){
	print_r($data);
	if ($die) {
		die();
	}
}

function debug_pr($data,$die=true){
	print"<pre>";
	print_r($data);
	print"</pre>";
	if ($die) {
		die();
	}
}


function outputCsv($fileName, $assocDataArray)
{
    ob_clean();
    header('Pragma: public');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private', false);
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment;filename=' . $fileName);    
    if(isset($assocDataArray['0'])){
        $fp = fopen('php://output', 'w');
        foreach($assocDataArray AS $values){
            fputcsv($fp, $values);
        }
        fclose($fp);
    }
    ob_flush();
}