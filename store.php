<?php
require __DIR__ ."/db.php";
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON,true);

store($input['data']);

echo json_encode($input['data']);
exit(0);