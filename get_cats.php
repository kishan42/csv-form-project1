<?php
$data = file_get_contents("categories.txt");
$rows = explode("\n", $data);
$categories = [];
foreach ($rows as $row) {
	$categories_data = explode(",", $row);
	$categories[trim($categories_data[0])]=trim($categories_data[1]);
}
echo json_encode(array("categories"=>$categories));
exit(0);