<!DOCTYPE html>
<html>
<head>
	<title>Form</title>

	<!--Bootstrap Styles-->
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<!--FontAwsome Styles-->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<style type="text/css">
		.cap-title{ 
		  text-transform: capitalize;
		}
	</style>
</head>
<body>
<div id="root" class="col-12">
	<h1 class="text-center">Csv Form</h1>
	<br />
	<form class="container">

	  <div class="form-group row">
	    <label class="col-4" for="input-lot-number">Lot Number</label>
	    <input type="text" :class="{'col-8 form-control':true, 'is-invalid':saveclicked && !valideLotNumber}" id="input-lot-number" placeholder="Enter lot number" v-model="input.lot_number">
	  </div>

	  <div class="form-group row" style="display: none;">
	    <label class="col-4" for="input-category">Category</label>
	    <select :class="{'col-8 form-control':true, 'is-invalid':saveclicked && !validCategory}" id="input-category" v-model="input.category">
	    	<option value="">Select Category</option>
	    	<option v-for="(category,key) in categories" :value="key" v-text="category"></option>
	    </select>
	  </div>

	  <div class="form-group row">
	    <label class="col-4" for="input-title">Title</label>
	    <input type="text" :class="{'col-8 form-control cap-title':true, 'is-invalid':saveclicked && !validTitle}" id="input-title" placeholder="Enter Title" v-model="input.title" v-on:change="capitalizeTitle">
	  </div>

	  <div class="form-group row">
	    <label class="col-4" for="input-sub-title">Sub Title</label>
	    <input type="text" :class="{'col-8 form-control':true, 'is-invalid':saveclicked && !validSubTitle}" id="input-sub-title" placeholder="Enter Sub Title" v-model="input.sub_title">
	  </div>

	  <div class="form-group row">
	    <label class="col-4" for="input-description">Description</label>
	    <textarea rows="12" :class="{'col-8 form-control':true, 'is-invalid':saveclicked && !validDescription}" id="input-description" placeholder="Enter Description" v-model="input.description"></textarea>
	  </div>

	  <div class="col-12 text-center">
	  	<button type="button" class="btn btn-success col-2" @click="addData">Add</button>
	  	
	  </div>

	</form>

	<div class="col-12">
		<div class="text-right">
			<a v-show="inputData.length" href="retrieve.php" class="btn btn-dark"><i class="fa fa-download"></i> Download</a>
			<button type="button" class="btn btn-danger" @click="deleteData"><i class="fa fa-trash"></i> Clear</button>
		</div>
		
		<br/>
		<table class="table">
			<thead>
				<th>Lot Number</th>
				<th>Category</th>
				<th>Title</th>
				<th>Sub Title</th>
				<th>Description</th>
			</thead>
			<tbody>
				<tr v-for="row in inputData">
					<td v-text="row.lot_number"></td>
					<td v-text="row.category"></td>
					<td v-text="row.title"></td>
					<td v-text="row.sub_title"></td>
					<td v-text="row.description"></td>
				</tr>
			</tbody>
		</table>
	</div>

</div>

<!--Scripts-->

	<!--Bootstrap Scripts-->
	<script src="js/jquery-3.3.1.min.js"anonymous"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--/Bootstrap Scripts-->

	<!--Axios.js-->
	<script src="js/axios.js"></script>

	<!--Vue.js-->
	<script src="js/vue.js"></script>

<!--/Scripts-->

<!--Custom Scripts-->
<script type="text/javascript">
	app = new Vue({
		el:'#root',
		data:{
			input:{
				category:''
			},
			inputData:[],
			categories:[],
			saveclicked:false,
		},
		watch:{
			'input.lot_number':function(){
				this.input.sub_title = this.input.lot_number;
			},
		},
		mounted:function () {
			this.getData();
			axios.get("get_cats.php").then(response => this.categories = response.data.categories).catch(error => console.log(error));
		},
		computed:{
			valideLotNumber:function valideLotNumber() {
				if (!this.input.lot_number) {
					return false;
				} else {
					return true;
				}
			},
			validCategory:function validCategory() {
				return true; //temporary disabled
				if (!this.input.category) {
					return false;
				} else {
					return true;
				}
			},
			validTitle:function validTitle() {
				if (!this.input.title) {
					return false;
				} else {
					return true;
				}
			},
			validSubTitle:function validSubTitle() {
				if (!this.input.sub_title) {
					return false;
				} else {
					return true;
				}
			},
			validDescription:function validDescription() {
				if (!this.input.description) {
					return false;
				} else {
					return true;
				}
			},
			validateForm:function validateForm() {
				return this.valideLotNumber && this.validCategory && this.validTitle && this.validSubTitle && this.validDescription;
			}
		},
		methods:{
			capitalizeTitle:function capitalizeTitle() {
				
				var spart = this.input.title.split(" ");
				for ( var i = 0; i < spart.length; i++ )
				{
					var j = spart[i].charAt(0).toUpperCase();
					spart[i] = j + spart[i].substr(1);
				}
				this.input.title = spart.join(" ");
			},
			getData:function getData() {
				axios.get("get.php")
				.then(response=>this.inputData = response.data);
			},
			addData:function addData() {
				this.saveclicked=true;
				if (!this.validateForm) {
					return false;
				}
				axios.post("store.php",{data:this.input})
				.then((response) => {
					this.inputData.push(response.data);
					this.saveclicked=false;
					this.input={
						category:''
					};
				})
				.catch(error => console.log(error));
			},
			deleteData:function deleteData() {
				if (!confirm("Are you sure want to clear entries?")) {
					return false;
				}
				axios.get("clear.php")
				.then(response => this.inputData=[]);
			},
		}
	});
</script>
</body>
</html>